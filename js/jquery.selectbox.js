/*
 *  Project: jQuery SelectBox
 *  Description: A jQuery-Plugin that replaces Browser-Default-Selectboxes and imitates their behaviour
 *  Author: Daniel Fuchs
 *  License: GPL
 *  Version: 0.9.6
 */

;(function ( $, window, document, undefined ) {

    var pluginName = "SelectBox", defaults = {
        classNames : {
            container : 'selectContainer',
            optionContainer : 'selectOptionContainer',
            option : 'selectOption',
            optGroup : 'selectOptGroup'
        },
        maxDropdownOptions : 8,
        multipleDefaultSize : 5
    };

    function SelectBox(element, options) {
        this.originalSelect = $(element);
        this.selectOptions = this.originalSelect.find('>*');

        this.options = $.extend({}, defaults, options);

        this.isMultiple = this.originalSelect.is('[multiple]');
        this.hasSize = this.originalSelect.is('[size]');
        this.isDisabled = this.originalSelect.is('[disabled]');
        this.size = parseInt(this.originalSelect.attr('size')) || this.options.multipleDefaultSize;

        this.isDefaultMode = this.hasSize === true && this.size > 1 ? false : true;

        this.container = $('<div/>', {
            'class' : 'selectBox ' + this.options.classNames.container + ' ' + (this.isDefaultMode ? 'default' : 'size')
        });

        if (this.isMultiple === true) {
            this.container.addClass('multiple');
        }

        this.selectOptionContainer = $('<ul/>', {
            'class' : this.options.classNames.optionContainer
        });

        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    SelectBox.prototype.init = function() {
        this.prepareHtml();
        this.addEventListeners();
    };

    SelectBox.prototype.prepareHtml = function() {
        if (this.isDisabled === true) {
            this.container.addClass('disabled');
        }

        if (this.isDefaultMode === true) {
            var hitBox = $('<div/>', {
                'class' : 'selectBoxHit'
            });

hitBox.append($('<ul/>').append(this.createOption(this.getSelectedItem())));
            hitBox.append($('<span/>', {
                'class' : 'hitBoxArrow'
            }));
            this.container.append(hitBox);
        }

        for ( var i = 0; i < this.selectOptions.length; i++) {
            var el = $(this.selectOptions[i]), append;

            if (el.is('optgroup')) {
                append = this.createOptGroup(el);
            } else {
                append = this.createOption(el);
            }

            this.selectOptionContainer.append(append);
        }

        this.originalSelect.hide();
        this.container.append(this.selectOptionContainer);

        if (this.isMultiple === true) {
            this.container.data('multiple', 'true');
        }

        if (this.size > 0) {
            this.container.data('size', this.size);
        }

        this.originalSelect.after(this.container);

        this.container.width(this.originalSelect.outerWidth());
        if (this.selectOptionContainer.width() < this.container.width()) {
this.selectOptionContainer.width(this.originalSelect.outerWidth());
        }

        var optionHeight = this.selectOptionContainer.find('li').outerHeight();
        this.selectOptionContainer.css('max-height', optionHeight * (this.size > 1 ? this.size : this.options.maxDropdownOptions));

        this.toggle();
    };

    SelectBox.prototype.addEventListeners = function() {
        if (this.isDisabled === false) {
            this.container.find('.selectBoxHit').on('click', $.proxy(this.onClick, this));
            this.selectOptionContainer.on('click', '.selectOption', $.proxy(this.onValueSelect, this));
        }
    };

    SelectBox.prototype.onClick = function(e) {
        var target = $(e.currentTarget);

        this.toggle();
    };

    SelectBox.prototype.onValueSelect = function(e) {
        var target = $(e.currentTarget), selectedValue;

        if (this.isMultiple && e.ctrlKey) {
            target.addClass('selected');
        } else {
this.selectOptionContainer.find('.selected').removeClass('selected');
        }

        target.toggleClass('selected');

        this.updateSelectBoxValue(target);

        this.originalSelect.trigger('change');

        this.toggle();
    };

    SelectBox.prototype.createOptGroup = function(optgroup) {
        var optGroupValue = $('<li/>', {
            'class' : 'optgroup ' + this.options.classNames.optGroup
        }), optGroupContainer = $('<ul/>', {
            'class' : this.options.classNames.optionContainer
        }), labelSpan = $('<div/>', {
            'class' : 'optGroupLabel'
        });

        labelSpan.text(optgroup.attr('label'));

        optGroupValue.append(labelSpan);
        var options = optgroup.find('option');

        for ( var i = 0; i < options.length; i++) {
            var option = $(options[i]);
            optGroupContainer.append(this.createOption(option));
        }

        optGroupValue.append(optGroupContainer);
        return optGroupValue;
    };

    SelectBox.prototype.createOption = function(option) {
        var optionContainer = $('<li/>', {
            'class' : this.options.classNames.option
        });

        optionContainer.text(option.text());
        optionContainer.data('value', option.val());

        return optionContainer;
    };

    SelectBox.prototype.getSelectedItem = function() {
        var selectedOption;
        for ( var i = 0; i < this.selectOptions.length; i++) {
            var option = $(this.selectOptions[i]);
            if (option.is('[selected]')) {
                selectedOption = option;
            }
        }

        if (selectedOption === undefined) {
            selectedOption = $(this.selectOptions[0]);
        }

        return selectedOption;
    };

    SelectBox.prototype.toggle = function(e) {
        if (this.isDefaultMode === true) {
            var status = this.selectOptionContainer.data('status');
            if (status === undefined || status === 'opened') {
                this.selectOptionContainer.hide();
                this.selectOptionContainer.data('status', 'closed');
//                $(document).off('click', $.proxy(this.onDocumentClick, this));
            } else {
                this.selectOptionContainer.show();
                this.selectOptionContainer.data('status', 'opened');
                $(document).on('click', $.proxy(this.onDocumentClick, this));
            }
        }
    };

    SelectBox.prototype.onDocumentClick = function(e) {
        var originalElement = e.srcElement || e.originalEvent.target || e.target, element = $(originalElement);

        if(!element.is(this.container.parent().find('*'))){
            this.close();
        }
    };

    SelectBox.prototype.updateSelectBoxValue = function(element) {
        var value = element.data('value');

        this.originalSelect.val(value);

        var selectedOptions = this.selectOptionContainer.find('.selected'), valueArr = [];

this.originalSelect.find('[selected]').removeAttr('selected');

        for ( var i = 0; i < selectedOptions.length; i++) {
            var tmpOption = $(selectedOptions[i]);

            valueArr.push(tmpOption.data('value'));
            this.originalSelect.find('[value="' + tmpOption.data('value') + '"]').attr('selected', 'selected');
        }

        this.originalSelect.val(valueArr);

        if (this.isDefaultMode === true) {
            var option = this.createOption(this.originalSelect.find('[value="' + value + '"]'));
            this.container.find('.selectBoxHit ul').html(option);
        }
    };

    SelectBox.prototype.close = function() {
        if(this.isDefaultMode === true){
            this.selectOptionContainer.hide();
            this.selectOptionContainer.data('status', 'closed');
        }
    };

    $.fn[pluginName] = function(options) {
        return this.each(function() {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new SelectBox(this, options));
            }
        });
    };

})(jQuery, window, document);