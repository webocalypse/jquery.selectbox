# jQuery.SelectBox.js
### A jQuery-Plugin that replaces Browser-Default-Selectboxes and imitates their behaviour
***

###Current Features
* Normal Selects are working
* Optgroups-Support
* Size-Attribute support
* Multiple-Select support
* Disabled-Mode is not supported in the current version, planned for version 1.0

***

###Version 0.9
* All Features working
* Disabled-Support is not implemented yet, planned for version 1.0

***
###Browser Support
* IE7 has a bug with select options that are bigger than the select
* IE 8/9 are supported
* The plugin is successfully tested in the latest version of Google Chrome and Firefox